<?php

/** @var yii\web\View $this */

$this->title = 'Aplicación Ciclistas';

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="site-index">

    <h3>Los ciclistas cuya edad está entre 25 y 35 años mostrando únicamente el dorsal y el nombre del cicista</h3>
    <?=    GridView::widget([
                'dataProvider'=>$dataProvider,
                'columns' =>[
                    'dorsal','nombre',
                 ],
                'layout'=>"{items}{pager}",
        ]);


    ?>
    
    <h3>Las etapas no circulares mostrando sólo el número de etapa y la longitud de las mismas</h3>

    <?=    GridView::widget([
                'dataProvider'=>$dataProvider2,
                'columns' =>[
                    'numetapa','kms',
                 ],
                'layout'=>"{items}",

           ]);


    ?>
    
    <h3>Los puertos con atura mayor a 1500 metros figurando el nombre del puero y el dorsal del ciclista que lo ganó</h3>
    <?=    GridView::widget([
                'dataProvider'=>$dataProvider3,
                'columns' =>[
                    'nompuerto','dorsal',
                 ],
                'layout'=>"{items}",

           ]);


    ?>
</div>
